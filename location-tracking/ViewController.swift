//
//  ViewController.swift
//  location-tracking
//
//  Created by Hoeng Linghor on 12/16/20.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    var previousLocation: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        // Do any additional setup after loading the view.
        if CLLocationManager.locationServicesEnabled(){
            setUpLocationManager()
            checkLocationAuthorization()
        }
    }

    func setUpLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    @IBAction func trackMe(_ sender: Any) {
        checkLocationAuthorization()
    }
    func startTrackinguserLocation(){
        self.mapView.showsUserLocation = true
        centerViewOnUserLocation()
        self.locationManager.startUpdatingLocation()
        self.previousLocation = getCenterLocation(mapView: mapView)
    }
    func centerViewOnUserLocation(){
        if let location = locationManager.location?.coordinate{
            let region = MKCoordinateRegion(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
            self.mapView.setRegion(region, animated: true)
        }
    }
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTrackinguserLocation()

            break
        case .authorizedAlways:
            break
        case .denied:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break

        default:
            break
        }
    }
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager){
        checkLocationAuthorization()
    }
    func getCenterLocation(mapView: MKMapView)-> CLLocation{
        let latitude = mapView.centerCoordinate.latitude
        let longtitude = mapView.centerCoordinate.longitude
        return CLLocation(latitude: latitude, longitude: longtitude)
    }
}

extension ViewController:MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = getCenterLocation(mapView: mapView)
        guard let previousLocation = self.previousLocation else {
            return
        }
        guard center.distance(from: previousLocation) > 50 else {
            return
        }
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(center) { (placeMark, error) in
            if let error = error{
                print(error.localizedDescription)
                return
            }
            if let placeMark = placeMark?.first{
                self.streetName.text = placeMark.thoroughfare ?? ""
                self.locationName.text = placeMark.name ?? ""
            }
        }
    }
}
